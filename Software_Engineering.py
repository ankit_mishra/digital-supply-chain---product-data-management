from flask import Flask,render_template,session,request,redirect,url_for,flash,g
import sqlite3

conn = sqlite3.connect('D:\delldb.db')#print "Opened database successfully"

conn.execute("DROP TABLE warehouse")
conn.execute("DROP TABLE resources")
conn.execute("CREATE TABLE resources (sid integer PRIMARY KEY AUTOINCREMENT, Country text UNIQUE, Wired integer,Bluetooth integer,Proximity integer)")
conn.execute("CREATE TABLE warehouse (sid integer PRIMARY KEY AUTOINCREMENT, Items text UNIQUE, Code text, Quantity integer, Job text)")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Wheel','WL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Infrared Light','IL',2,'CD')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Right Button','MB',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Bluetooth Sensor','BS',2,'CD')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Proximity Sensor','PS',2,'CD')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Port','PO',2,'CD')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Left Button','MB',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Axle or Shaft','AX',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Ball','BL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Switch','SW',2,'CD')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Middle Click','MCL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Left Click','LCL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Right Click','BL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Scroll Bar','SWL',2,'MFJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Colour:Black','COL',2,'PJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Colour:White','COL',2,'PJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Colour:Grey','COL',2,'PJ')")
conn.execute("INSERT INTO warehouse (Items,Code,Quantity,Job) VALUES ('Colour:Metallic-Grey','COL',2,'PJ')")
conn.execute("INSERT INTO resources (Country,Wired,Bluetooth,Proximity) VALUES ('USA',1,1,1)")
conn.execute("INSERT INTO resources (Country,Wired,Bluetooth,Proximity) VALUES ('India',1,0,1)")
conn.execute("INSERT INTO resources (Country,Wired,Bluetooth,Proximity) VALUES ('Africa',1,0,0)")
conn.commit()

app = Flask(__name__)
app.secret_key = "any random string"
items=[]
bom=[]
quant=[]
country=["USA","India","Africa"]
mboms=[]
@app.route('/')
def ebom():
    var = conn.execute("SELECT * FROM warehouse")
    for row in var:
        print (row[0],row[1],row[2],row[3],row[4])
    varr = conn.execute("SELECT Proximity FROM resources where Country='USA'")
    for row in varr:
        print(row[0])#,row[1],row[2],row[3],row[4])

    return render_template("ebommouse.html")
@app.route('/ebom_to_mbom',methods=['GET','POST'])
def ebom_to_mbom():
    if request.method == 'GET':
        items.append(request.args.get('external_1'))
        items.append(request.args.get('external_2'))
        items.append(request.args.get('external_3'))
        items.append(request.args.get('external_4'))
        items.append(request.args.get('external_5'))
        items.append(request.args.get('internal_1'))
        items.append(request.args.get('internal_2'))
        items.append(request.args.get('internal_3'))
        items.append(request.args.get('internal_4'))
        items.append(request.args.get('internal_5'))
        items.append(request.args.get('type'))
        quant.append(int(request.args.get('external_1_quant')))
        quant.append(int(request.args.get('external_2_quant')))
        quant.append(int(request.args.get('external_3_quant')))
        quant.append(int(request.args.get('external_4_quant')))
        quant.append(int(request.args.get('external_5_quant')))
        quant.append(int(request.args.get('internal_1_quant')))
        quant.append(int(request.args.get('internal_2_quant')))
        quant.append(int(request.args.get('internal_3_quant')))
        quant.append(int(request.args.get('internal_4_quant')))
        quant.append(int(request.args.get('internal_5_quant')))
        for k in items:
            print (k)
        for j in country:
            query = "SELECT "+items[10]+" FROM resources WHERE Country=(?)"
            vari=conn.execute(query,[j])
            bom=[]
            for var in vari:
                print (var[0])
                if var[0]==1:
                    c=0
                    for i in items:
                        if i=="Wired" or i=="Bluetooth" or i=="Proximity":
                            bom.append(i)
                        else:
                            if i!='#':
                                print (i)
                                varr=conn.execute("SELECT Quantity FROM warehouse WHERE Items=(?)",[i])
                                for str in varr:
                                    if str[0]>=quant[c]:
                                        bom.append(i)
                                    else:
                                        print ("Cannot Fulfill Demand")
                                        return ("505")
                            else:
                                pass
                        c+=1
                    mboms.append(bom)
                else:
                    c = 0
                    for i in items:
                        if i == "Bluetooth" and j=="India":
                            bom.append("Proximity")
                            for replace in range(len(bom)):
                                if bom[replace]=="Bluetooth Sensor":
                                    bom[replace]="Proximity Sensor"
                        if i=="Bluetooth" and j=="Africa":
                            bom.append("Wired")
                            for replace in range(len(bom)):
                                if bom[replace]=="Bluetooth Sensor" or bom[replace]=="Proximity Sensor":
                                    bom[replace]="Port"
                        if i=="Proximity" and j=="Africa":
                            bom.append("Wired")
                            for replace in range(len(bom)):
                                if bom[replace]=="Bluetooth Sensor" or bom[replace]=="Proximity Sensor":
                                    bom[replace]="Port"
                        else:
                            if i!='#':
                                varr=conn.execute("SELECT Quantity FROM warehouse WHERE Items=(?)",[i])
                                for str in varr:
                                    if str[0]>quant[c]:
                                        bom.append(i)
                                    else:
                                        print ("Cannot Fulfill Demand")
                                        return ("505")
                            else:
                                pass
                        c += 1
                    mboms.append(bom)
        print ("---------------------------------")
        for iter in range(len(mboms)):
            print ("********")
            print (mboms[iter])
        return render_template("mbom_1.html")
@app.route('/mbom',methods=['GET','POST'])
def mbom():
    if request.method == "POST":
        country=request.args.get('country')
        if country=="USA":
            bom=mboms[0]





if __name__ == '__main__':
    app.run()